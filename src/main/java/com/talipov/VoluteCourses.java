package com.talipov;

/**
 * Created by marsel on 23.03.17.
 */
public interface VoluteCourses {
    Float convert(Float sum);
}
