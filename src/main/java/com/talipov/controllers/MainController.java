package com.talipov.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by marsel on 23.03.17.
 */
@Controller
public class MainController {
    @GetMapping(value = "/home")
    public String home() {
        return "/home";
    }
}
